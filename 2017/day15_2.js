class Generator {
  constructor(start, factor, modulo, checkModulo) {
    this.lastValue = start;
    this.factor = factor;
    this.modulo = modulo;
    this.checkModulo = checkModulo;
  }

  nextVal() {
    do {
      this.lastValue = (this.lastValue * this.factor) % this.modulo;
    } while (this.lastValue % this.checkModulo !== 0);

    return this.lastValue;
  }
}

const Gen1 = new Generator(699, 16807, 2147483647, 4);
const Gen2 = new Generator(124, 48271, 2147483647, 8);

let matches = 0;
for(let i = 0; i < 5000000; i++) {// noprotect
  if(
    Gen1.nextVal().toString(2).toString().slice(-16) === Gen2.nextVal().toString(2).toString().slice(-16)
  ) {
    matches++;
  }
}

console.debug(matches);