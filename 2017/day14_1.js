Array.prototype.chunk = function(length) {
  return this.reduce((l, v, k) => {
    if(k % length === 0) l[l.length] = [v]; else l[l.length - 1].push(v);
      return l;
  }, []);
};

function knothash(input) {

  input = input.split('').map(val => val.charCodeAt(0)).concat([17,31,73,47,23]);

  let pos = 0,
      skip = 0,
      list = [...Array(256).keys()];

  // Compute sparse hash
  for(let i = 0; i < 64; i++) {

    input.forEach(length => {
      
      const overbuffer = Math.max(0, length - (list.length - pos));

      const els = list.splice(pos, length - overbuffer)
        .concat(list.splice(0, overbuffer))
        .reverse();

      list = els.slice(length - overbuffer)
        .concat(list.slice(0, pos))
        .concat(els.slice(0, length - overbuffer))
        .concat(list.slice(pos));

      pos = (pos + length + skip++) % list.length;

    });
    
  }

  // Compute dense hash then make an hexa of it
  return list.chunk(16).reduce(
    (t, l) => t + ("00" + l.reduce(
      (h, v) => h ^ v, 0
    ).toString(16)).slice(-2), ""
  );

}

// Make an array of 128, create line by prepending key, compute knothash of each line
// Then convert each hex in knothash to binary, compute the number of 1 and sums up the array
console.debug([...Array(128).keys()].map(i => {
  return knothash('xlqgujun-'+i).split('').map(n => {
    return parseInt(n, 16).toString(2);
  }).join('').split('1').length - 1;
}).reduce((t, n) => t + n, 0));