let input = 325489;

// Determine which circle the number is in
// 1: [2, 9] 2: [10, 25] 3: [26, 49] 4: [50, 81]
let circle = 0;
let min = 1;
let max = 1;
do {
  circle++;
  min = max + 1;
  max = min + 7 + (circle - 1) * 8;
} while(max < input);

// Now determine distance from the number in the center of each side
let right = 1,
    top = 1,
    left = 1,
    bottom = 1;
for(let i = 0; i < circle; i++) {
  right += i * 8 + 1;
  top += i * 8 + 3;
  left += i * 8 + 5;
  bottom += i * 8 + 7;
}
let distance = Math.min(
  Math.abs(input - right),
  Math.abs(input - top),
  Math.abs(input - left),
  Math.abs(input - bottom)
);

document.writeln(circle + distance);