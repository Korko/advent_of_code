Array.prototype.chunk = function(length) {
  return this.reduce((l, v, k) => {
    if(k % length === 0) l[l.length] = [v]; else l[l.length - 1].push(v);
      return l;
  }, []);
};

function knothash(input) {

  input = input.split('').map(val => val.charCodeAt(0)).concat([17,31,73,47,23]);

  let pos = 0,
      skip = 0,
      list = [...Array(256).keys()];

  // Compute sparse hash
  for(let i = 0; i < 64; i++) {

    input.forEach(length => {
      
      const overbuffer = Math.max(0, length - (list.length - pos));

      const els = list.splice(pos, length - overbuffer)
        .concat(list.splice(0, overbuffer))
        .reverse();

      list = els.slice(length - overbuffer)
        .concat(list.slice(0, pos))
        .concat(els.slice(0, length - overbuffer))
        .concat(list.slice(pos));

      pos = (pos + length + skip++) % list.length;

    });
    
  }

  // Compute dense hash then make an hexa of it
  return list.chunk(16).reduce(
    (t, l) => t + ("00" + l.reduce(
      (h, v) => h ^ v, 0
    ).toString(16)).slice(-2), ""
  );

}

// Make an array of 128, create line by prepending key, compute knothash of each line
// Then convert each hex in knothash to binary
let hashs = [...Array(128).keys()].map(i => {
  return knothash('xlqgujun-'+i).split('').map(n => {
    return ("0000" + parseInt(n, 16).toString(2)).slice(-4);
  }).join('').split('').map(n => parseInt(n, 2));
});

let region = 2;
hashs.forEach((line, i) => {
  line.forEach((char, j) => {
    if(char === 1) {
      markRegion(hashs, i, j, region++);
    }
  });
});

function markRegion(hashs, i, j, region) {
  hashs[i][j] = region;

  if ((i > 0) && (hashs[i-1][j] === 1)) markRegion(hashs, i-1, j, region);
  if ((j > 0) && (hashs[i][j-1] === 1)) markRegion(hashs, i, j-1, region);
  if ((i < hashs.length - 1) && (hashs[i+1][j] === 1)) markRegion(hashs, i+1, j, region);
  if ((j < hashs[0].length - 1) && (hashs[i][j+1] === 1)) markRegion(hashs, i, j+1, region);
}

console.debug(region - 2);