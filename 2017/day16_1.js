const input = document.body.innerText.split(',');

let programs = [...Array(16).keys()].map((v, pos) => String.fromCharCode(pos + 97));

input.forEach(action => {
  let args, tmp;
  switch(action[0]) {
    case 's':
      args = /s(\d+)/.exec(action);
      programs = programs.splice(-args[1]).concat(programs);
      break;
    case 'x':
      args = /x(\d+)\/(\d+)/.exec(action);
      tmp = programs[args[1]];
      programs[args[1]] = programs[args[2]];
      programs[args[2]] = tmp;
      break;
    case 'p':
      args = /p(\w)\/(\w)/.exec(action);
      tmp = programs.indexOf(args[1]);
      programs[programs.indexOf(args[2])] = args[1];
      programs[tmp] = args[2];
      break;
  }
});

console.debug(programs.join(''));