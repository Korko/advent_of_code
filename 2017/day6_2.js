let input = [4,10,4,1,8,4,9,14,5,1,14,15,0,15,3,5];

let loops = 0;
let checks = [];
while(checks.indexOf(input.join('.')) === -1) { // noprotect
  loops++;
  checks.push(input.join('.'));

  let max = input.indexOf(Math.max.apply(null,input));
  let total = input[max];
  input[max] = 0;

  let i = (max + 1) % input.length;
  while(total > 0) {
    input[i]++
    total--;
    
    i = (i + 1) % input.length;
  }
}

document.write(checks.length - checks.indexOf(input.join('.')));


