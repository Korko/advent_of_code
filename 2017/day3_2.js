let input = 325489;

(function() {
let layer = 0, x = 0, y = 0;
let circle = [[1]];

while(1) {

  layer++;
  for(let k = 0; k < circle.length; k++) {
    circle[k].unshift(0);
    circle[k].push(0);
  }
  circle.unshift([0]);
  circle.push([0]);

  for(let side = 0; side < 4; side++) {
    // Numbers on each side
    let max = (side !== 4 ? (layer * 2) : (1 + layer * 2));
    for(let n = 0; n < max; n++) {
      switch(side) {
        case 0:
          x = max;
          y = max - n - 1;
          break;
        case 1:
          x = max - n - 1;
          y = 0;
          break;
        case 2:
          x = 0;
          y = n + 1;
          break;
        case 3:
          x = n + 1;
          y = max;
          break;
      }

      value = val(circle, x-1, y-1) + val(circle, x-1, y) + val(circle, x-1, y+1) + val(circle, x, y+1) + val(circle, x+1, y+1) + val(circle, x+1, y) + val(circle, x+1, y-1) + val(circle, x, y-1);

      circle[x] = circle[x] === undefined ? [0] : circle[x];
      circle[x][y] = value;

      if(value >= input) {
        matrix(circle);
        document.write(value);
        return;
      }
    }
  }
}
})()

function val(arr, x, y) {
  return arr[x] === undefined ? 0 : (arr[x][y] === undefined ? 0 : arr[x][y]);
}

function matrix(arr) {
  console.debug('----');
  for(let y = 0; y < arr.length; y++) {
    let narr = [];
    for(let x = 0; x < arr.length; x++) {
      narr[x] = arr[x] ? (arr[x][y] ? arr[x][y] : 0) : 0;
    }
    console.debug(narr);
  }
  console.debug('----');
}