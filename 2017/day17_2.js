const input = 376;

let buffer = {'next': null, 'length': 1}, pos = 0;
for(let i = 1; i < 50000001; i++) {//noprotect
  pos = ((pos + input) % buffer.length) + 1;
  buffer.length++;

  if(pos === 1) {
    buffer.next = i;
  }
}

console.debug(buffer.next);