const input = 376;

let buffer = [0];
let pos = 0;
for(let i = 1; i < 2018; i++) {//noprotect
  pos = ((pos + input) % buffer.length) + 1;
  buffer = buffer.slice(0, pos).concat([i], buffer.slice(pos));
}

console.debug(buffer[(pos+1)%buffer.length]);