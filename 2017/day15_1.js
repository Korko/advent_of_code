class Generator {
  constructor(start, factor, modulo) {
    this.lastValue = start;
    this.factor = factor;
    this.modulo = modulo;
  }

  nextVal() {
    this.lastValue = (this.lastValue * this.factor) % this.modulo;
    return this.lastValue;
  }
}

const Gen1 = new Generator(699, 16807, 2147483647);
const Gen2 = new Generator(124, 48271, 2147483647);

let matches = 0;
for(let i = 0; i < 40000000; i++) {//noprotect
  if(
    Gen1.nextVal().toString(2).toString().slice(-16) === Gen2.nextVal().toString(2).toString().slice(-16)
  ) {
    matches++;
  }
}

console.debug(matches);