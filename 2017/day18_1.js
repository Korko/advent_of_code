const instructions = document.body.innerText.split("\n");

function handleInstructions(instructions) {
  let registers = new Proxy({}, {
      get: (obj, prop) => (prop in obj) ? obj[prop] : 0
    }),
    lastSound;

  for(let i = 0; i < instructions.length; i++) {//noprotect
    if((match = /(snd|set|add|mul|mod|rcv|jgz) ([a-z]|-?\d+)(?: ([a-z]|-?\d+))?/.exec(instructions[i])) !== null) {
      switch(match[1]) {
        case 'set':
          registers[match[2]] = isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'snd':
          lastSound = registers[match[2]]; break;
        case 'add':
          registers[match[2]] += isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'mul':
          registers[match[2]] *= isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'mod':
          registers[match[2]] %= isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'rcv':
          if(registers[match[2]] !== 0) return lastSound;
        case 'jgz':
          if((isNaN(match[2]) && registers[match[2]] > 0) || (!isNaN(match[2]) && parseInt(match[2], 10) > 0)) i += parseInt(match[3], 10) - 1; break;
      }
    }
  }
}

console.debug(handleInstructions(instructions));