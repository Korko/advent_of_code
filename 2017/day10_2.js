const input = "120,93,0,90,5,80,129,74,1,165,204,255,254,2,50,113".split("").map(val => val.charCodeAt(0)).concat([17,31,73,47,23]);
let list = [...Array(256).keys()];

let pos = 0,
    skip = 0;

for(let i = 0; i < 64; i++) {
  input.forEach(length => {
    const overbuffer = Math.max(0, length - (list.length - pos));

    const els = list.splice(pos, length - overbuffer)
      .concat(list.splice(0, overbuffer))
      .reverse();

    list = els.slice(length - overbuffer)
      .concat(list.slice(0, pos))
      .concat(els.slice(0, length - overbuffer))
      .concat(list.slice(pos));

    pos = (pos + length + skip++) % list.length;
  });
}

Array.prototype.chunk = function(length) {
  return this.reduce((l, v, k) => {
    if(k % length === 0) l[l.length] = [v]; else l[l.length - 1].push(v);
      return l;
  }, []);
};

console.debug(list.chunk(16).reduce(
  (t, l) => t + ("00" + l.reduce(
    (h, v) => h ^ v, 0
  ).toString(16)).slice(-2), ""
));