let input = "120,93,0,90,5,80,129,74,1,165,204,255,254,2,50,113".split(",").map(val => parseInt(val, 10));
let list = [...Array(256).keys()];

let pos = 0;
let skip = 0;
input.forEach(length => {
  let overbuffer = Math.max(0, length - (list.length - pos));

  let els = list.splice(pos, length - overbuffer)
    .concat(list.splice(0, overbuffer))
    .reverse();

  list = els.slice(length - overbuffer)
    .concat(list.slice(0, pos))
    .concat(els.slice(0, length - overbuffer))
    .concat(list.slice(pos));

  pos = (pos + length + skip) % list.length;
  skip++;
});

console.debug(list, list[0] * list[1]);