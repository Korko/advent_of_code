console.debug(
  danceloop(
    [...Array(16).keys()].map((v, pos) => String.fromCharCode(pos + 97)),
    document.body.innerText.split(','),
    1000000000
  ).join('')
);

function danceloop(programs, input, loopSize) {
  
  const programsStart = programs.join('');

  let breakLoopSize = 1;
  for(let i = 0; i < loopSize; i++) {// noprotect
    programs = dance(programs, input);
  
    if(programs.join('') === programsStart) {
      breakLoopSize = i;
      break;
    }
  }

  const finalLoopSize = loopSize % (breakLoopSize +1);
  for(let i = 0; i < finalLoopSize; i++) {// noprotect
    programs = dance(programs, input);
  }

  return programs;
  
}

function dance(programs, input) {
  
  input.forEach(action => {
    let args, tmp;
    switch(action[0]) {
      case 's':
        args = /s(\d+)/.exec(action);
        programs = programs.splice(-args[1]).concat(programs);
        break;
      case 'x':
        args = /x(\d+)\/(\d+)/.exec(action);
        tmp = programs[args[1]];
        programs[args[1]] = programs[args[2]];
        programs[args[2]] = tmp;
        break;
      case 'p':
        args = /p(\w)\/(\w)/.exec(action);
        tmp = programs.indexOf(args[1]);
        programs[programs.indexOf(args[2])] = args[1];
        programs[tmp] = args[2];
        break;
    }
  });
  
  return programs;
  
}