const instructions = ["set i 31",
"set a 1",
"mul p 17",
"jgz p p",
"mul a 2",
"add i -1",
"jgz i -2",
"add a -1",
"set i 127",
"set p 952",
"mul p 8505",
"mod p a",
"mul p 129749",
"add p 12345",
"mod p a",
"set b p",
"mod b 10000",
"snd b",
"add i -1",
"jgz i -9",
"jgz a 3",
"rcv b",
"jgz b -1",
"set f 0",
"set i 126",
"rcv a",
"rcv b",
"set p a",
"mul p -1",
"add p b",
"jgz p 4",
"snd a",
"set a b",
"jgz 1 3",
"snd b",
"set f 1",
"add i -1",
"jgz i -11",
"snd a",
"jgz f -16",
"jgz a -19"];

let wait = {
  length: function() { return [...Object.values(this)].reduce((s, v) => s += (v === true) ? 1 : 0, 0); }
};

function waitPipe(pipe, id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if(pipe.length > 0) {
        wait[id] = false;
        resolve(pipe.shift());
      } else if(wait.length() === 2) {
        reject('deadlock');
      } else {if(wait[id] !== true)
        wait[id] = true;
        waitPipe(pipe, id).then(resolve, reject);
      }
    }, 0);
  });
}

async function handleInstructions(id, instructions, pipeR, pipeS) {
  let registers = new Proxy({p: id}, {
    get: (obj, prop) => (prop in obj) ? obj[prop] : 0 // Get 0 if value not defined yet
  });

  for(let i = 0; i < instructions.length; i++) {//noprotect
    let match;
    if((match = /(snd|set|add|mul|mod|rcv|jgz) (-?\d+|\w)(?: (-?\d+|\w))?/.exec(instructions[i])) !== null) {
      switch(match[1]) {
        case 'set':
          registers[match[2]] = isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'snd':
          (isNaN(match[2])) ? pipeS.push(registers[match[2]]) : pipeS.push(parseInt(match[2], 10)); break;
        case 'add':
          registers[match[2]] += isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'mul':
          registers[match[2]] *= isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'mod':
          registers[match[2]] %= isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10); break;
        case 'jgz':
          if((isNaN(match[2]) && registers[match[2]] > 0) || (!isNaN(match[2]) && parseInt(match[2], 10) > 0)) i += (isNaN(match[3]) ? registers[match[3]] : parseInt(match[3], 10)) - 1;
          break;
        case 'rcv':
          registers[match[2]] = await waitPipe(pipeR, id); break;
      }
    }
  }
}

let total1 = 0,
    pipe1 = [],
    pipe2 = [];

Promise.race([
  handleInstructions(
    0,
    instructions,
    pipe1,
    pipe2
  ),
  handleInstructions(
    1,
    instructions,
    pipe2,
    new Proxy(pipe1, { set: function(obj, prop, value) { obj[prop] = value; if(prop !== 'length') { total1++; } return true; } })
  )
]).then(() => console.debug(total1), (error) => console.debug(total1));
